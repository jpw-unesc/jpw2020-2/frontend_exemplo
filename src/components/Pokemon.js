import React from 'react'
import FormPokemon from './FormPokemon'
import ListaPokemon from './ListaPokemon'
import Axios from 'axios'

export default class Pokemon extends React.Component{

    constructor(props){
        super(props)

        this.API_ENDPOINT = "http://localhost:8080/pokemons"

        this.state = {
            "pokemons": [],
            "selecionado": null
        }
    }

    componentDidMount = () => {
        this.getAllPokemon()
    }

    getAllPokemon = () => {
        var requisicao = Axios.get(this.API_ENDPOINT)
        requisicao.then((resposta) => {
            if(resposta.status == 200){
                this.setState({
                    "pokemons": resposta.data
                })
            }
        })
    }

    savePokemon = (pokemon) => {
        var requisicao = Axios.post(this.API_ENDPOINT, pokemon)
        requisicao.then((resposta) => {
            if(resposta.status == 200){
                this.getAllPokemon()
            }
        })
    }

    deletePokemon = (pokemonId) => {
        var requisicao = Axios.delete(this.API_ENDPOINT + "/" + pokemonId)
        requisicao.then((resposta) => {
            if(resposta.status == 200){
                this.getAllPokemon()
            }
        })
    }

    putPokemon = (pokemon) => {
        var requisicao = Axios.put(this.API_ENDPOINT + "/" + this.state.selecionado._id, pokemon)
        requisicao.then((resposta) => {
            if(resposta.status == 200){
                this.getAllPokemon()
            }
        })
    }

    selectPokemon = (pokemon) => {
        if(this.state.selecionado == pokemon){
            this.setState({
                "selecionado": null
            })
        } else {
            this.setState({
                "selecionado": pokemon
            })
        }
    }

    render(){

        var selecionado = this.state.selecionado ? this.state.selecionado._id : null

        return(
            <main>
            <section>
                <h2>Formulario</h2>
                {selecionado}
                <FormPokemon
                    save={this.savePokemon}
                    put={this.putPokemon}
                    selecionado={this.state.selecionado}
                    key={selecionado}
                >
                </FormPokemon>
            </section>
            <section>
                <h2>Lista</h2>
                <ListaPokemon
                    pokemons={this.state.pokemons}
                    delete={this.deletePokemon}
                    select={this.selectPokemon}>
                </ListaPokemon>
            </section>
        </main>
        )
    }
}