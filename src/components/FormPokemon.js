import React from 'react'

export default class FormPokemon extends React.Component{
    constructor(props){
        super(props)

        if(this.props.selecionado){
            this.state = this.props.selecionado
        } else {
            this.state = {
                "nome": "",
                "numero": 0
            }
        }
    }

    handleInput = (event) => {
        this.setState({
            [event.target.id]: event.target.value
        })
    }

    setPokemon = () => {
        if(this.props.selecionado){
            this.props.put(this.state)
        }else{
            this.props.save(this.state)
        }

        this.setState({
            "nome": "",
            "numero": 0
        })
    }

    render(){
        return(
            <form>
            <input
                type="text"
                id="nome"
                value={this.state.nome}
                onChange={this.handleInput}>
            </input>
            <input
                type="number"
                id="numero"
                value={this.state.numero}
                onChange={this.handleInput}>
            </input>
            <button
                type="button"
                onClick={this.setPokemon}>
                Inserir
            </button>
        </form>
        )
    }
}